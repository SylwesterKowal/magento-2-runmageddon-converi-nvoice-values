<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RunmageddonConverInvoiceValues\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConvertValues extends Command
{

    const ORDER_ID = "order_id";
    const STORE_ID = "store_id";

    public function __construct(
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Api\OrderItemRepositoryInterface            $orderItemRepository,
        \Magento\Framework\App\State                               $state,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender      $invoiceSender
    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderItemRepository = $orderItemRepository;
        $this->state = $state;
        $this->invoiceSender = $invoiceSender;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {

        try {
            $this->state->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $ex) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        }

        if (!$storee_it = $input->getArgument(self::STORE_ID)) {
            echo "Proszę podać Store ID jako pierwszy parametr" . PHP_EOL;
        }

        if ($order_id = $input->getArgument(self::ORDER_ID)) {
            $output->writeln("TEST order_id " . $order_id);
            $output->writeln("store_id " . $input->getArgument(self::STORE_ID));

            $collection = $this->getOrdersCollection(
                [
                    'entity_id' => ['eq' => $order_id],
                    'store_id' => $storee_it,
//                    'applied_rule_ids' => [['finset' => ['479']]] // tylko gdy jest zastosowana reguła na kosych "Vouchery RMG"
                ]
            );
            foreach ($collection as $order) {
                $this->convert($order);
            }
        } else {

            $now = new \DateTime();
            $fromdate = $now->format('2024-03-01 00:00:00');

            $collection = $this->getOrdersCollection(
                [
                    'state' => ['in' => ['processing', 'complete']],
                    'rmg_converted' => ['null' => true, 'eq' => ''],
                    'created_at' => ['gteq' => $fromdate],
                    'store_id' => $storee_it,
                    'applied_rule_ids' => [['finset' => ['479']]] // tylko gdy jest zastosowana reguła na kosych "Vouchery RMG"
                ]
            );


            foreach ($collection as $order) {
                $output->writeln($order->getEntityId());

                $this->convert($order);
            }
        }

        return 1;

    }

    public function convert($order)
    {

        if (!$order->getPayment()) {
            return;
        }


        $couponCode = $order->getCouponCode();
        $couponCodePrefix = substr($couponCode, 0, 2);
        $curentStatus = $order->getStatus();
        $curentState = $order->getState();

        switch ($couponCodePrefix) {
            case 'SM':
                $voucher = 19;
                $rabat = 81;
                break;
            case 'R5':
                $voucher = 20.12;
                $rabat = 79.88;
                break;
        }

        $baseSubtotalInclTaxAfterDiscount = 0;
        foreach ($order->getAllVisibleItems() as $orderItem) {
            if ($orderItem->getProductType() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                $childrenItems = $orderItem->getChildrenItems();
                foreach ($childrenItems as $childrenItem) {
                    $discountAmount = (float)$childrenItem->getRowTotalInclTax() * ($childrenItem->getDiscountPercent() / 100);
                    $baseSubtotalInclTaxAfterDiscount += $childrenItem->getRowTotalInclTax() - $discountAmount;
                }

            } else {
                $discountAmount = (float)$orderItem->getRowTotalInclTax() * ($orderItem->getDiscountPercent() / 100);
                $baseSubtotalInclTaxAfterDiscount += $orderItem->getRowTotalInclTax() - $discountAmount;
            }
        }

        $discountAmoutTotalOrder = 0;
        $voucherRabat = ($baseSubtotalInclTaxAfterDiscount < $rabat) ? $baseSubtotalInclTaxAfterDiscount - $voucher : $rabat;
        $voucherValue = $voucher;
        foreach ($order->getAllItems() as $orderItem) {

            if ($orderItem->getProductType() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                continue;
            }

            $discountAmount = (float)$orderItem->getRowTotalInclTax() * ($orderItem->getDiscountPercent() / 100);
            //$voucherDiscount = $voucherRabat * (($orderItem->getRowTotalInclTax() - $discountAmount) / $baseSubtotalInclTaxAfterDiscount);
            $voucherDiscount = ($baseSubtotalInclTaxAfterDiscount == 0) ? 0 : $voucherRabat * (($orderItem->getRowTotalInclTax() - $discountAmount) / $baseSubtotalInclTaxAfterDiscount);
            $orderItem->setDiscountAmount($discountAmount + $voucherDiscount)
                ->setBaseDiscountAmount($discountAmount + $voucherDiscount)
                ->setDiscountInvoiced($discountAmount + $voucherDiscount)
                ->setBaseDiscountInvoiced($discountAmount + $voucherDiscount)
                ->save();

            $discountAmoutTotalOrder += $discountAmount + $voucherDiscount;
        }

        foreach ($order->getInvoiceCollection() as $invoice) {
            $baseSubtotalInclTaxAfterDiscount = 0;
            foreach ($invoice->getAllItems() as $invoiceItem) {
                $orderItemCollection = $this->orderItemRepository->get($invoiceItem->getOrderItemId());
                $discountAmount = (float)$invoiceItem->getRowTotalInclTax() * ($orderItemCollection->getDiscountPercent() / 100);
                $baseSubtotalInclTaxAfterDiscount += $invoiceItem->getRowTotalInclTax() - $discountAmount;
            }

            $discountAmoutTotal = 0;
            $voucherRabat = ($baseSubtotalInclTaxAfterDiscount < $rabat) ? $baseSubtotalInclTaxAfterDiscount - $voucher : $rabat;
            $voucherValue = $voucher;

            foreach ($invoice->getAllItems() as $invoiceItem) {
                $orderItemCollection = $this->orderItemRepository->get($invoiceItem->getOrderItemId());
                $discountAmount = (float)$invoiceItem->getRowTotalInclTax() * ($orderItemCollection->getDiscountPercent() / 100);


                $voucherDiscount = ($baseSubtotalInclTaxAfterDiscount == 0) ? 0 : $voucherRabat * (($invoiceItem->getRowTotalInclTax() - $discountAmount) / $baseSubtotalInclTaxAfterDiscount);

                $invoiceItem->setDiscountAmount($discountAmount + $voucherDiscount)
                    ->setBaseDiscountAmount($discountAmount + $voucherDiscount)
                    ->save();
                $discountAmoutTotal += $discountAmount + $voucherDiscount;
            }

            $GrandTotal = ($invoice->getSubtotalInclTax() - $discountAmoutTotal + $invoice->getBaseShippingAmount()) - $voucherValue;
            $invoice->setDiscountAmount($discountAmoutTotal * -1);
            $invoice->setBaseDiscountAmount($discountAmoutTotal * -1);
            $invoice->setBaseGrandTotal($GrandTotal);
            $invoice->setGrandTotal($GrandTotal);
            $invoice->save();


        }

        $GrandTotal = ($order->getBaseSubtotalInclTax() - $discountAmoutTotalOrder + $order->getBaseShippingAmount()) - $voucherValue;
        $order->setBaseDiscountAmount($discountAmoutTotalOrder * -1);
        $order->setBaseDiscountInvoiced($discountAmoutTotalOrder * -1);
        $order->setDiscountAmount($discountAmoutTotalOrder * -1);
        $order->setDiscountInvoiced($discountAmoutTotalOrder * -1);
        $order->setBaseGrandTotal($GrandTotal);
        $order->setGrandTotal($GrandTotal);
        $order->setBaseTotalInvoiced($GrandTotal);
        $order->setBaseTotalPaid($GrandTotal);
        $order->setTotalInvoiced($GrandTotal);
        $order->setTotalPaid($GrandTotal);

        // add order comment
        $order->addStatusHistoryComment(
            'Przeliczenie faktury i zamówienia z voucherem i wysłanie do klienta faktury',
            true
        );

        $order->setRmgConverted(1)
            ->save();

        $order->setState($curentState);
        $order->setStatus($curentStatus);
        $order->save();
        // send invoice email
        try {
//            $this->invoiceSender->send($invoice);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

    }

    public function getOrdersCollection(array $filters = [])
    {

        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }


        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_runmageddonconverinvoicevalues:convertvalues");
        $this->setDescription("Convert Values");
        $this->setDefinition([
            new InputArgument(self::STORE_ID, InputArgument::OPTIONAL, "Store ID"),
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Order ID")

        ]);
        parent::configure();
    }
}

